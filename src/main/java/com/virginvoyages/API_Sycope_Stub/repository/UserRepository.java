package com.virginvoyages.API_Sycope_Stub.repository;

import org.springframework.data.repository.CrudRepository;

import com.virginvoyages.API_Sycope_Stub.entity.UserInfoEntity;

public interface UserRepository extends CrudRepository<UserInfoEntity, String> {

}