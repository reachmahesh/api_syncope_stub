package com.virginvoyages.API_Sycope_Stub.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;

@Entity
@Table(name = "USER_INFO")
public class UserInfoEntity{

	 protected UserInfoEntity() {
	    }
	   @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Long id;

	@NotNull
	   @Size(max = 150)
	   @Column(name = "username")
	private String username;
	

	@Null
	   @Size(max = 150)
	   @Column(name = "last_name")
	private String last_name;
	
	@Null
	   @Size(max = 150)
	   @Column(name = "first_name")
	private String first_name;
	
	@Null
	   @Size(max = 150)
	   @Column(name = "email_id")
	private String email_id;


	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	public UserInfoEntity(@NotNull @Size(max = 150) String username, @Null @Size(max = 150) String last_name,
			@Null @Size(max = 150) String first_name, @Null @Size(max = 150) String email_id) {
		this.username = username;
		this.last_name = last_name;
		this.first_name = first_name;
		this.email_id = email_id;
	}
	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", username=" + username + ", last_name=" + last_name + ", first_name="
				+ first_name + ", email_id=" + email_id + "]";
	}

	
}