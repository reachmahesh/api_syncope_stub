package com.virginvoyages.API_Sycope_Stub.Service;


import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.virginvoyages.API_Sycope_Stub.dao.UserInfoDAO;
import com.virginvoyages.API_Sycope_Stub.model.UserInfo;

@Service
public class UserService {
	
	private static final Logger log = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private UserInfoDAO userInfoDao;
	
	// Add new User
	public String addUser(UserInfo s) {
		
		try {
			userInfoDao.addUser(s);
			return "saved";
		} catch(Exception e) {
			log.debug("Exception in addUser for a given UserInfo",  e);
			return "failed";
		}
	}


        // Update a User
	public String updateUser(String username, UserInfo s) {
		try {
			userInfoDao.updateUser(username, s);
			return "Updated";
		}catch(Exception e) {
			 log.debug("Exception in updateUser for a given username = {}", username, e);
			return "Failed";
		}
	}


	// Get all Users
	public List<UserInfo> getAllUser(){
		return userInfoDao.getAllUser();
	}

	
	// Get User by Id
	public Optional<UserInfo> getUser(String username) {
		return userInfoDao.getUser(username);
	}

	
	// Delete a User
	public String deleteUser(String username) {
		try{
			userInfoDao.deleteUser(username);
			return "Deleted";
		}catch(Exception e) {
			 log.debug("Exception in deleteUser a given username = {}", username, e);
			return "Failed";
		}
	}

	
}