package com.virginvoyages.API_Sycope_Stub.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.virginvoyages.API_Sycope_Stub.dao.UserInfoDAO;
import com.virginvoyages.API_Sycope_Stub.model.UserInfo;



@Repository
public class UserInfoDAOImpl implements UserInfoDAO {

	private static final Logger log = LoggerFactory.getLogger(UserInfoDAOImpl.class);
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int addUser(UserInfo s) {
		
		int size = 0;
		if (!getUser(s.getUsername()).isPresent()) {
			size = jdbcTemplate.update(
					"insert into User_Info (username, first_name, last_name, email_id) values(?,?,?,?)",
					s.getUsername().trim(), s.getFirst_name(), s.getLast_name(),s.getEmail_id());
		}
		return size;
	}


	@Override
	public int updateUser(String username, UserInfo s) {
		int size = 0;
			size = jdbcTemplate.update(
					"update User_Info set username=?,first_name=?, last_name=?,email_id=? where username=?",
					s.getUsername().trim(), s.getLast_name(), s.getFirst_name(),s.getEmail_id(),username);
		
		return size;
	}

	@Override
	public List<UserInfo> getAllUser() {
		log.info("getUser Query: select * from User_Info");

            List<UserInfo> result = new ArrayList();
            try {
                result = jdbcTemplate.query(
                        "select * from User_Info", new UserMapper());
            } catch (IncorrectResultSizeDataAccessException e) {
                log.debug("Expected to find exactly one UserInfo", e);
            }

            return result;
	}

	class UserMapper implements RowMapper<UserInfo>{
		@Override
		public UserInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
			UserInfo userInfo = new UserInfo();
			userInfo.setUsername(rs.getString("username"));
			userInfo.setFirst_name(rs.getString("first_name"));
			userInfo.setLast_name(rs.getString("last_name"));
			userInfo.setEmail_id(rs.getString("email_id"));
			return userInfo;
		}
	}
	@Override
	public int deleteUser(String username) {
		int size = 0;
		log.info("delete from User_Info where upper(username) ="
				+ username.toUpperCase().trim());	
		size = jdbcTemplate.update(
				"delete from User_Info where upper(username) = ?",username.toUpperCase().trim());
		return size;
	}

	@Override
	public Optional<UserInfo> getUser(String username) {
		log.info("getUser Query" + "select * from User_Info where upper(username)="
				+ username.toUpperCase().trim());
            UserInfo result = null;
            try {
                result = jdbcTemplate.queryForObject(
                        "select * from User_Info where upper(username) = ?",
                        new Object[] { username.toUpperCase().trim() },
                        (rs, rowNum) -> new UserInfo(
                                rs.getString("username"), 
                                rs.getString("first_name"), 
                                rs.getString("last_name"),
                                rs.getString("email_id")));
            } catch (IncorrectResultSizeDataAccessException e) {
                log.debug("Expected to find exactly one UserInfo for a given username = {}", username, e);
            }

            return Optional.ofNullable(result);
	}

}
