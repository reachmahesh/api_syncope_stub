package com.virginvoyages.API_Sycope_Stub.dao;

import java.util.List;
import java.util.Optional;

import com.virginvoyages.API_Sycope_Stub.model.UserInfo;

public interface UserInfoDAO {

	int addUser(UserInfo s);

	int updateUser(String username, UserInfo s);

	List<UserInfo> getAllUser();

	int deleteUser(String username);

	Optional<UserInfo> getUser(String username);

}
