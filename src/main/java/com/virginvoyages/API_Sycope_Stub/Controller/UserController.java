package com.virginvoyages.API_Sycope_Stub.Controller;


import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.virginvoyages.API_Sycope_Stub.Service.UserService;
import com.virginvoyages.API_Sycope_Stub.model.UserInfo;

@Controller
@RequestMapping(path="/userinfo")
public class UserController {
	private static final Logger log = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;
	
	// Add new user
	@PostMapping(path="/adduser")
	public @ResponseBody String addNewUser (@RequestBody UserInfo userInfo) {
		return userService.addUser(userInfo);
	}
	
	// Get all users
	@GetMapping(path="/displayall")
	public @ResponseBody List<UserInfo> getAllUser() {
		return userService.getAllUser();
	}
	
	// Get single User by Id
	@GetMapping(path="/get/{username}")
	public @ResponseBody ResponseEntity<UserInfo> getUserById(@PathVariable(name = "username") String username) {
		
		Optional<UserInfo> userInfo = userService.getUser(username);
		if (userInfo.isPresent()) {
			log.info("User Exists");
			return new ResponseEntity(userInfo, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	// Update a User
	@PostMapping(path="/update/{username}")
	public @ResponseBody String updateUser(@PathVariable(name = "username") String username, @RequestBody 
        UserInfo userInfo) {
		return userService.updateUser(username,userInfo);
	}

	
	// Delete a User
	@DeleteMapping(path="/delete/{username}")
	public @ResponseBody String deleteUser(@PathVariable(name = "username") String username) {
		return userService.deleteUser(username);
	}
}