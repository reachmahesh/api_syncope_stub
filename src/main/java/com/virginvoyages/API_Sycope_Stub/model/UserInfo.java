package com.virginvoyages.API_Sycope_Stub.model;

public class UserInfo {

	public UserInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String username;
	private String last_name;
	private String first_name;
	private String email_id;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public UserInfo(String username, String last_name, String first_name, String email_id) {
		super();
		this.username = username;
		this.last_name = last_name;
		this.first_name = first_name;
		this.email_id = email_id;
	}

	@Override
	public String toString() {
		return "UserInfo [username=" + username + ", last_name=" + last_name + ", first_name=" + first_name
				+ ", email_id=" + email_id + "]";
	}

}