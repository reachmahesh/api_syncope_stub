package com.virginvoyages.API_Sycope_Stub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSycopeStubApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSycopeStubApplication.class, args);
	}

}
